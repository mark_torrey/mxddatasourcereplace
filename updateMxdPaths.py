#
# Parameter Info
# - 0 : Path of directory containing mxds
# - 1 : Path to new sde file
# - 2 : New database name
# - 3 : New database schema name

import arcpy
import os
import sys

inputDirectory = arcpy.GetParameterAsText(0)
if not os.path.exists(inputDirectory):
    arcpy.AddMessage("Input directory does not exist")
    sys.exit()

newSDEPath = arcpy.GetParameterAsText(1)
if not os.path.exists(newSDEPath) or not os.path.isfile(newSDEPath):
    arcpy.AddMessage("New sde connection file is invalid")
    sys.exit()

newDataBaseName = arcpy.GetParameterAsText(2)
if not newDataBaseName or len(newDatabasename) <= 0:
    arcpy.AddMessage("Invalid database name parameter")
    sys.exit()

newSchemaName = arcpy.GetParameterAsText(3)
if not newSchemaName or len(newSchemaName) <= 0:
    arcpy.AddMessage("Invalid schema name parameter")
    sys.exit()

#find all the MXD's in the directory tree
for root, subFolders, files in os.walk(inputDirectory):
    for mxdfilename in files:
        mxdfullpath = os.path.join(root, mxdfilename)
        mxdbasename, mxdextension = os.path.splitext(mxdfullpath)
        if mxdextension.lower() == ".mxd":
            arcpy.AddMessage("*****")
            arcpy.AddMessage("opening {0}".format(mxdfilename))
            #open the map document
            mapdocument = arcpy.mapping.MapDocument(mxdfullpath)
            #get all the layersI'
            for lyr in arcpy.mapping.ListLayers(mapdocument):
                #get the source from the layer
                if lyr.supports("workspacePath"):
                    wrksppath = lyr.workspacePath
                    basename, extension = os.path.splitext(wrksppath)
                    if extension.lower() == ".sde":
                        #replace the old path wih the new
                        arcpy.AddMessage("Switching layer {0} to use {1}".format(lyr, newSDEPath))
                        newDatasetname = "{0}.{1}.{2}".format(newDataBaseName, newSchemaName, lyr.datasetName.split(".")[2])
                        newFQDN = "{0}\{1}".format(newSDEPath, newDatasetname)
                        if (arcpy.Exists(newFQDN)):
                            lyr.replaceDataSource(newSDEPath, "SDE_WORKSPACE", newDatasetname)
                        else:
                            arcpy.AddWarning("{0} Does not exist in {1}".format(newDatasetname, newSDEPath))
                        
                    else:
                        arcpy.AddMessage("layer {0} does is not referencing an sde connection".format(wrksppath))
            #save your changes
            mapdocument.save()
            del mapdocument